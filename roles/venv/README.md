appstore_s3
=========

Create and configure Dev AppStore S3 bucket.

Requirements
------------

Boto and boto3


Role Variables
--------------

* AWS key, key id, region
* ec2_name


Dependencies
------------

Assumes the following roles were previously run and
that a Dev AppStore called ec2_name exists:

* provision_ec2
* clone_appstore


Example Playbook
----------------

---
- hosts: localhost
  vars_files:
    - group_vars/common.yml
    - group_vars/secrets.yml
  roles:
    - appstore_host

- hosts: appstores
  vars_files:
    - group_vars/common.yml
    - group_vars/secrets.yml
  roles:
    - role: appstore_venv

